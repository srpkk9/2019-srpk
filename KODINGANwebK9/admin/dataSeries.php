<?php
date_default_timezone_set('Asia/Jakarta');

$nama_lokasi = "";

$chart = array(
  'caption'=>'Grafik Pemantauan',
  'subCaption'=>'Data Curah Hujan',
  'numdisplaysets'=>'10',
  'labeldisplay'=>'rotate',
  'showRealTimeValue'=>'1',
  'theme'=>$_GET['theme'],
  'plotToolText'=>'$label<br>Data Hujan: <b>$dataValue</b>',
  'setAdaptiveYMin'=>'1'	
);

$categories = array(
  array(
    'category'=>array(
      array(
        'label'=>date('Y-m-d H:i:s')
      )
    )
  )			
);

$dataset = array(
  array(
    'data'=>array(
      array(
        'value'=>0
        )
    )
   )
);	

echo json_encode(
  array(
    'chart'=>$chart,
    'categories'=>$categories,
    'dataset'=>$dataset
  )
);

?>