<?php
error_reporting(0);
include("check.php");
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>REPEKO | Dashboard</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="plugins/iCheck/flat/blue.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="plugins/morris/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="plugins/jvectormap/jquery-jvectormap-1.2.2.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="plugins/datepicker/datepicker3.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="plugins/daterangepicker/daterangepicker.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition skin-green sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="index2.html" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>R</b>PK</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>REPEKO</b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="dist/img/user2-160x160.jpg" class="user-image" alt="User Image">
              <span class="hidden-xs"><?php echo $_SESSION['nama_op']; ?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">

                <p>
                <?php echo $_SESSION['nama_op']; ?>
                  <small><?php echo $_SESSION['jabatan']; ?></small>
                </p>
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-right">
                  <a href="logout.php" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <!--div class="user-panel">
        <div class="pull-left image">
          <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>Alexander Pierce</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div-->
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <!--li class="header">MAIN NAVIGATION</li-->
        <li class=""><a href="daftar_operator.php"><i class="fa fa-user"></i> <span>Daftar Operator</span></a></li>
        <li class=""><a href="hasil_pemantauan.php"><i class="fa fa-book"></i> <span>Hasil Pemantauan</span></a></li>
        <li class="active"><a href="data_lokasi.php"><i class="fa fa-map"></i> <span>Data Lokasi</span></a></li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Data Lokasi
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Data Lokasi</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
    <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Lokasi</h3>
                <div class="pull-right">
                <?php if ($pengentri == "ya") { ?>
                  <a href="form_add_lokasi.php" class="btn btn-success btn-md"><i class="fa fa-plus fa-sm"></i> Tambah Lokasi</a>
                <?php } ?>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="dataLokasi" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>ID Lokasi</th>
                  <th>ID Operator</th>
                  <th>Nama Lokasi</th>
                  <th>Koordinat Lokasi</th>
                  <th>Start Pemasangan</th>
                  <th>Cetak pdf</th>
                  <th>Cek data</th>
                  <?php if ($pengentri == "ya") { ?>
                  <th></th>
                  <?php } ?>
                </tr>
                </thead>
                <tbody>
                <?php
                  include "koneksi.php";
                  $sql = "SELECT * FROM tabel_lokasi";
                  $res = mysqli_query($koneksi, $sql);
                  while ($row = mysqli_fetch_assoc($res)) {
                ?>
                <tr>
                  <td><?php echo $row['id_lokasi'];?></td>
                  <td><?php echo $row["id_operator"];?></td>
                  <td><?php echo $row["nama_lokasi"];?></td>
                  <td><?php echo $row["koordinat_lokasi"];?></td>
                  <td><?php echo $row["start_pemasangan"];?></td>
                  <td>
                    <a class="btn btn-success btn-sm" href="cetak_pdf.php?id=<?php echo $row["id_lokasi"]; ?>"><i class="fa fa-download fa-md"></i></a>
                  </td>
                  <td>
                    <a class="btn btn-success btn-sm" href="chart_gunung.php?id=<?php echo $row["id_lokasi"]; ?>"><i class="fa fa-line-chart fa-md"></i></a>
                    <a class="btn btn-primary btn-sm" href="table_gunung.php?id=<?php echo $row["id_lokasi"]; ?>"><i class="fa fa-table fa-md"></i></a>
                  </td>
                  <?php if ($pengentri == "ya") { ?>
                  <td>
                    <a class="btn btn-warning btn-sm" href="form_edit_lokasi.php?id=<?php echo $row["id_lokasi"]; ?>"><i class="fa fa-cog fa-md"></i></a>
                    <a class="btn btn-danger btn-sm" data-placement="right" data-toggle="modal" href="#deleteModal" onclick="set_url('delete_lokasi.php?id=<?php echo $row['id_lokasi']; ?>');"><i class="fa fa-trash fa-md"></i></a>
                  </td>
                  <?php } ?>
                </tr>
                <?php
                  }
                ?>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

          <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
            <div class="modal-dialog" style="background: #021e4f">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h4 class="modal-title">Konfirmasi</h4>
                </div>
                <div class="modal-body">
                  <h2>Apakah Anda yakin, data akan <b>dihapus</b>?</h2>
                </div>
                <div class="modal-footer">
                  <a class="btn btn-danger" id="btn-delete">Hapus</a>
                  <button type="button" class="btn btn-success" data-dismiss="modal">Tidak</button>
                </div>
              </div>
            </div>
          </div>


        </div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Teknik</b> Komputer
    </div>
    <strong>Copyright &copy; Kelompok B2.9</strong>
  </footer>

</div>
<!-- ./wrapper -->

<script>
  function set_url(url) {
    $('#btn-delete').attr('href',url);
  }
</script>
<!-- jQuery 2.2.3 -->
<script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.6 -->
<script src="bootstrap/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="plugins/morris/morris.min.js"></script>
<!-- Sparkline -->
<script src="plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="plugins/knob/jquery.knob.js"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="plugins/daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/app.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
</body>
</html>
