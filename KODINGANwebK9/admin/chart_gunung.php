<?php
error_reporting(0);
include("check.php");
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>REPEKO | Dashboard</title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
  <link rel="stylesheet" href="plugins/iCheck/flat/blue.css">
  <link rel="stylesheet" href="plugins/morris/morris.css">
  <link rel="stylesheet" href="plugins/jvectormap/jquery-jvectormap-1.2.2.css">
  <link rel="stylesheet" href="plugins/datepicker/datepicker3.css">
  <link rel="stylesheet" href="plugins/daterangepicker/daterangepicker.css">
  <link rel="stylesheet" href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

  <script src="dist/js/fusioncharts.js"></script>
  <script src="dist/js/themes/fusioncharts.theme.fusion.js"></script>
  
  <script type="text/javascript" src="jquery.min.js"></script>
  <script>
    FusionCharts.ready(
      function(){
        var theme = window.document.getElementById('theme');
        var type = window.document.getElementById('type');
        var url = new URL(window.location.href);
        var id = url.searchParams.get("id");

        function renderChart(theme,type){
          var chart = new FusionCharts(
            {
              type:type,
              dataFormat:'jsonurl',
              renderAt:'chart',
              dataSource:'dataSeries.php?theme='+theme,
              events:{
                rendered: function(evt,arg){
                  var chartRef = evt.sender;
                  function updateData(){
                    /*
                    var t = new Date();
                    var date = t.getHours()+":"+t.getMinutes()+":"+t.getSeconds(); 
                    
                    var max = 999;
                    var min = 0;	 				
                    */

                    var today = new Date();
                    var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
                    var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
                    var tgl = date+' '+time;
                    //var max = 999;
                    //var min = 0;
                    var val1 = Math.floor(Math.random()*(1300-1100+1)+1100);
                    var val2 = Math.floor(Math.random()*(65-55+1)+55);

                    /*Insert Data - Realtime */

                    /* BEGIN */
                    $.ajax({
                      type: "POST",
                      url: "reload_chart.php?id="+id,
                      data: "data_curah_hujan=" + val1+ "&data_tekanan_udara=" + val2+ "&waktu=" + tgl,
                    });
                    /* END */

                    strData = "&label="+ tgl +"&value="+val1;
                    chartRef.feedData(strData);
                  }
                  
                  chartRef.intervalUpdateId = setInterval(updateData,10000);
                },
                disposed: function(evt,arg){
                  clearInterval(evt.sender.intervalUpdateId);
                }	
              }	
            }	
            );
            
            chart.render();
          }
          
          theme.onchange =  function (){
            renderChart(theme.value, type.value);
            //alert(type.value)
            //renderChart(theme.value, "bulb");
          }
          
          type.onchange =  function (){
            theme.onchange();
          }
          
          theme.onchange();
        }
      );	
  </script>
</head>
<body class="hold-transition skin-green sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <a href="index2.html" class="logo">
      <span class="logo-mini"><b>R</b>PK</span>
      <span class="logo-lg"><b>REPEKO</b></span>
    </a>
    <nav class="navbar navbar-static-top">
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="dist/img/user2-160x160.jpg" class="user-image" alt="User Image">
              <span class="hidden-xs"><?php echo $_SESSION['nama_op']; ?></span>
            </a>
            <ul class="dropdown-menu">
              <li class="user-header">
                <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">

                <p>
                <?php echo $_SESSION['nama_op']; ?>
                  <small><?php echo $_SESSION['jabatan']; ?></small>
                </p>
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-right">
                  <a href="logout.php" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
        </ul>
      </div>
    </nav>
  </header>

  <aside class="main-sidebar">
    <section class="sidebar">
      <ul class="sidebar-menu">
        <li class=""><a href="daftar_operator.php"><i class="fa fa-user"></i> <span>Daftar Operator</span></a></li>
        <li class=""><a href="hasil_pemantauan.php"><i class="fa fa-book"></i> <span>Hasil Pemantauan</span></a></li>
        <li class="active"><a href="data_lokasi.php"><i class="fa fa-map"></i> <span>Data Lokasi</span></a></li>
      </ul>
    </section>
  </aside>

  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        Realtime Chart
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Realtime Chart</li>
      </ol>
    </section>

    <section class="content">
      <div class="row">
        <section class="col-lg-7">
          <select id="theme" hidden>
            <option value="fusion">fusion</option>
          </select>

          <select id="type" hidden>
            <option value="realtimeline" type="hidden">Line</option>
          </select>
          <div id="chart"></div>
        </section>

      </div>

      <br>
      <div class="row">
        <section class="col-md-6">
        <a href="data_lokasi.php" class="btn btn-warning btn-md">Kembali</a>
        </section>
      </div>

    </section>
  </div>
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Teknik</b> Komputer
    </div>
    <strong>Copyright &copy; Kelompok B2.9</strong>
  </footer>

</div>

<script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<script src="bootstrap/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="plugins/morris/morris.min.js"></script>
<script src="plugins/sparkline/jquery.sparkline.min.js"></script>
<script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<script src="plugins/knob/jquery.knob.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="plugins/daterangepicker/daterangepicker.js"></script>
<script src="plugins/datepicker/bootstrap-datepicker.js"></script>
<script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
<script src="plugins/fastclick/fastclick.js"></script>
<script src="dist/js/app.min.js"></script>
<script src="dist/js/pages/dashboard.js"></script>
<script src="dist/js/demo.js"></script>
</body>
</html>
