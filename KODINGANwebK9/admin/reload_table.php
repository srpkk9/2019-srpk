<?php


include "koneksi.php";

$id = $_GET['id'];

if ($koneksi->connect_error) {
    die("Connection failed: " . $koneksi->connect_error);
}

$sql = "SELECT * FROM tabel_pemantauan WHERE id_lokasi = $id ORDER BY waktu DESC";
$result = mysqli_query($koneksi, $sql);
 
$riwayat = '<table class="table table-bordered table-hover">
          <thead>
        <tr>
          <th>Nomor</th>
          <th>ID Pemantauan</th>
          <th>ID Lokasi</th>
          <th>Data Curah Hujan</th>
          <th>Data Tekanan Udara</th>
          <th>Waktu</th>
        </tr>
        </thead>
        <tbody>';

$i = 1;
  
while ($row = mysqli_fetch_assoc($result)) {
  $riwayat .= '<tr>
          <td>'.($i++).'</td>
          <td>'.$row['id_pemantauan'].'</td>
          <td>'.$row['id_lokasi'].'</td>
          <td>'.$row['data_curah_hujan'].'</td>
          <td>'.$row['data_tekanan_udara'].'</td>
          <td>'.$row['waktu'].'</td>
        </tr>';
}

if (!mysqli_num_rows($result)) {
  $riwayat .= '<tr>
        <td colspan="4">Data kosong</td>
      </tr>';
}

$riwayat .= '</tbody></table>';

$sql = "SELECT * FROM tabel_pemantauan WHERE id_lokasi = $id ORDER BY waktu DESC LIMIT 1";
$result = mysqli_query($koneksi, $sql);

$current = "";
$data_curah_hujan = "";
$date = "";

while ($row = mysqli_fetch_assoc($result)) {
    $current = "
    <ul>
    <li>ID Pemantauan: ".$row["id_pemantauan"]."</li>
    <li>ID Lokasi: ".$row["id_lokasi"]."</li>
    <li>Data Curah Hujan: ".$row["data_curah_hujan"]."</li>
    <li>Data Tekanan Udara: ".$row["data_tekanan_udara"]."</li>
    <li>Waktu: ".$row["waktu"]."</li>
      </ul>
    ";
    $data_curah_hujan = $row["data_curah_hujan"];
    $date = $row["waktu"];
};
  
/*Insert Data - Realtime */

/* BEGIN */
$val1 = floor(mt_rand(0,mt_getrandmax()-1)/mt_getrandmax()*(1300-1100+1)+1100);
$val2 = floor(mt_rand(0,mt_getrandmax()-1)/mt_getrandmax()*(999-0+1)+0);
$date = date('Y-m-d H:i:s');

$sql = "INSERT INTO tabel_pemantauan(id_lokasi,data_curah_hujan,data_tekanan_udara,waktu)
          VALUES ('".$id."',
                  '".$val1."',
                  '".$val2."',
                  '".$date."'
      )";
/* END */

$result = mysqli_query($koneksi, $sql);

$response = array(
  'riwayat'=>$riwayat,
  'current' => $current,
  'hujan' => $data_curah_hujan,
  'date' => $date
  );

echo json_encode($response);
?>