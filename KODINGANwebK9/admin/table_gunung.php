<?php
error_reporting(0);
include("check.php");
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>REPEKO | Dashboard</title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
  <link rel="stylesheet" href="plugins/iCheck/flat/blue.css">
  <link rel="stylesheet" href="plugins/morris/morris.css">
  <link rel="stylesheet" href="plugins/jvectormap/jquery-jvectormap-1.2.2.css">
  <link rel="stylesheet" href="plugins/datepicker/datepicker3.css">
  <link rel="stylesheet" href="plugins/daterangepicker/daterangepicker.css">
  <link rel="stylesheet" href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

  <script type="text/javascript" src="jquery.min.js"></script>
  <script type="text/javascript">
		$(document).ready(
			function(){
				function getRandom(min,max){
					return (Math.random()*(max-min+1)+min);
				}
        var url = new URL(window.location.href);
        var id = url.searchParams.get("id");
				var time = setInterval(
					function(){
						$.ajax(
							{
							  url:'reload_table.php?id='+id,
							  //data:{val:getRandom(0,999)},
							  method:'post',
							  dataType:'json',
							  success: function(response){
                  $('#riwayat').html(response.riwayat);
                  $('#current').html(response.current);
								}	
							}	
						);
          },10000
        );
			}
    );
    </script>
</head>
<body class="hold-transition skin-green sidebar-mini">
<div class="wrapper">

<header class="main-header">

    <a href="index2.html" class="logo">
      <span class="logo-mini"><b>R</b>PK</span>
      <span class="logo-lg"><b>REPEKO</b></span>
    </a>
    <nav class="navbar navbar-static-top">
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
      <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
      <ul class="nav navbar-nav">
        <li class="dropdown user user-menu">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
          <img src="dist/img/user2-160x160.jpg" class="user-image" alt="User Image">
          <span class="hidden-xs"><?php echo $_SESSION['nama_op']; ?></span>
        </a>
        <ul class="dropdown-menu">
          <li class="user-header">
          <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">

          <p>
          <?php echo $_SESSION['nama_op']; ?>
            <small><?php echo $_SESSION['jabatan']; ?></small>
          </p>
          </li>
          <li class="user-footer">
          <div class="pull-right">
            <a href="logout.php" class="btn btn-default btn-flat">Sign out</a>
          </div>
          </li>
        </ul>
        </li>
      </ul>
      </div>
    </nav>
  </header>
  
  <aside class="main-sidebar">
    <section class="sidebar">
      <ul class="sidebar-menu">
        <li><a href="daftar_operator.php"><i class="fa fa-user"></i> <span>Daftar Operator</span></a></li>
        <li class="active"><a href="hasil_pemantauan.php"><i class="fa fa-book"></i> <span>Hasil Pemantauan</span></a></li>
        <li><a href="data_lokasi.php"><i class="fa fa-map"></i> <span>Data Lokasi</span></a></li>
      </ul>
    </section>
  </aside>

  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        Tabel Gunung
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Tabel Gunung</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
    <div class="row">

        <div class="col-md-4">
              <div class="box box-solid">
                <div class="box-header with-border">
                  <i class="fa fa-circle-o-notch"></i>

                  <h3 class="box-title">Data Gunung saat ini:</h3>
                </div>
                <!-- /.box-header -->
                <div id="current" class="box-body">
                  <ul>
                    <li>ID Pemantauan: Memuat Data</li>
                    <li>ID Lokasi: Memuat Data</li>
                    <li>Data Curah Hujan: Memuat Data</li>
                    <li>Data Tekanan Udara: Memuat Data</li>
                    <li>Waktu: Memuat Data</li>
                  </ul>
                </div>
              </div>
        </div>

        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Hasil Pemantauan</h3>
                <div class="pull-right">
                    <a href="data_lokasi.php" class="btn btn-warning btn-md">Kembali</a>
                </div>
            </div>
            
            <div class="box-body">

              <div id='riwayat'>
                <table class="table table-bordered table-hover">
                  <thead>
                    <tr>
                      <th>Nomor</th>
                      <th>ID Pemantauan</th>
                      <th>ID Lokasi</th>
                      <th>Data Curah Hujan</th>
                      <th>Data Tekanan Udara</th>
                      <th>Waktu</th>
                    </tr>
                    <tr>
                      <td>Memuat Data</td>
                      <td>Memuat Data</td>
                      <td>Memuat Data</td>
                      <td>Memuat Data</td>
                      <td>Memuat Data</td>
                      <td>Memuat Data</td>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>

            </div>
          </div>
        </div>

    </section>
  </div>
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Teknik</b> Komputer
    </div>
    <strong>Copyright &copy; Kelompok B2.9</strong>
  </footer>

</div>
<script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<script src="bootstrap/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="plugins/morris/morris.min.js"></script>
<script src="plugins/sparkline/jquery.sparkline.min.js"></script>
<script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<script src="plugins/knob/jquery.knob.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="plugins/daterangepicker/daterangepicker.js"></script>
<script src="plugins/datepicker/bootstrap-datepicker.js"></script>
<script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
<script src="plugins/fastclick/fastclick.js"></script>
<script src="dist/js/app.min.js"></script>
<script src="dist/js/pages/dashboard.js"></script>
<script src="dist/js/demo.js"></script>
</body>
</html>
