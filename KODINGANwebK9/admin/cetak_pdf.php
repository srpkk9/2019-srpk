<?php

require_once('../tcpdf/tcpdf.php');

// create new PDF document
//$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
//$pdf = new TCPDF ('L', 'mm', array('201','150'), true, 'UTF-8', false);
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);

//$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetMargins(0, 3, 0);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, 1);

// add a page
$pdf->AddPage();

// set font
$pdf->SetFont('helvetica', '', 12);


function fetch_data() {

$id_lokasi = $_GET['id'];
$output = '';	
/*
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "id10523185_db_kopi2";
*/
include("koneksi.php");

//$koneksi = mysqli_connect($servername, $username, $password, $dbname);
// Check connection

$sql = "SELECT * FROM tabel_pemantauan WHERE id_lokasi = '$id_lokasi' ORDER BY waktu DESC";
$result = mysqli_query($koneksi, $sql);

    // output data of each row
    $i = 1;
    while($row = mysqli_fetch_array($result)) {   

    $output .= '<tr>
                <td align="center">'.$i++.'</td>
    			<td align="center">'.$row['id_pemantauan'].'</td>
    			<td align="center">'.$row['data_curah_hujan'].'</td>
    			<td align="center">'.$row['data_tekanan_udara'].'</td>
    			<td align="center">'.$row['waktu'].'</td>
    			</tr>';
    
    }
    return $output;
}

include("koneksi.php");
$q = 'SELECT nama_lokasi FROM tabel_lokasi WHERE id_lokasi = '.$_GET['id'].'';
$res = mysqli_query($koneksi, $q);
$data = mysqli_fetch_assoc($res);

$content  = '';  
$content .= ' 

<h1>Data '.$data["nama_lokasi"].'</h1>
<table border="1">  
  <tr>
  <th align="center"><b>No</b></th>
  <th align="center"><b>ID Pemantauan</b></th>
  <th align="center"><b>Data Hujan</b></th>
  <th align="center"><b>Data Udara</b></th>
  <th align="center"><b>Waktu</b></th>
  </tr>';     
   
$content .= fetch_data(); 
$content .= '
</table>';


// output the HTML content
$pdf->writeHTML($content, true, true, true, true, '');

// reset pointer to the last page
$pdf->lastPage();
// ---------------------------------------------------------

//Close and output PDF document
$pdf->Output('data_'.strtolower($data["nama_lokasi"]).'.pdf', 'I');
/*
require_once('../TCPDF/tcpdf.php');
//konfigurasi TCPDF
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
//menambahkan halaman
$pdf->AddPage();
//isi pdf
$teks = <<<EOD
<h1>Assalamu'alaikum Wr Wb</h1>

Dipagi yang cerah ini, seperti biasa kita harus selalu mensyukuri nikmat yang Allah berikan agar apa yang kita lakukan menjadi lebih bermakna dan selalu dalam keridhoannya.

<h1>Wassalamu'alaikum Wr Wb</h1>
EOD;
//print teks
$pdf->writeHTMLCell(0, 0, '', '', $teks, 0, 1, 0, true, '', true);
//hasil print
$pdf->Output('buatpdf.pdf','I');
*/
?>