-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 07 Nov 2019 pada 17.00
-- Versi server: 10.4.6-MariaDB
-- Versi PHP: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `id10523185_db_kopi`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `tabel_lokasi`
--

CREATE TABLE `tabel_lokasi` (
  `id_lokasi` int(11) NOT NULL,
  `id_operator` int(11) NOT NULL,
  `nama_lokasi` varchar(30) NOT NULL,
  `koordinat_lokasi` varchar(10) NOT NULL,
  `start_pemasangan` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tabel_lokasi`
--

INSERT INTO `tabel_lokasi` (`id_lokasi`, `id_operator`, `nama_lokasi`, `koordinat_lokasi`, `start_pemasangan`) VALUES
(100, 2, 'Gunung Salak', 'Bogor', '2019-11-03 08:21:29'),
(101, 3, 'Gunung Gede', 'Bogor', '2019-11-01 02:59:59'),
(102, 4, 'Gunung Semeru', 'Malang', '2019-11-07 04:12:15'),
(103, 5, 'Gunung Papandayan', 'Garut', '2019-11-07 01:01:59'),
(105, 4, 'Gunung Tampomas', 'Sumedang', '2019-11-07 02:02:22');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tabel_operator`
--

CREATE TABLE `tabel_operator` (
  `id_op` int(11) NOT NULL,
  `nama_op` varchar(30) NOT NULL,
  `pass_op` varchar(255) NOT NULL,
  `alamat` varchar(30) NOT NULL,
  `jabatan` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tabel_operator`
--

INSERT INTO `tabel_operator` (`id_op`, `nama_op`, `pass_op`, `alamat`, `jabatan`) VALUES
(1, 'Bima', '$2y$10$wgyafCtVcO4TkloegK/J2OoL0M2n5oCNL.yKlpVWUOkX04onPF53q', 'Bogor', 'Pengentri'),
(2, 'Irsa', '$2y$10$c6U9o74OEGVPZSbQdfRBquJBk4AfSGXdtG064tSOAdeUaBcXf31t2', 'Jakarta', 'Pemantau'),
(3, 'Ray', '$2y$10$c6U9o74OEGVPZSbQdfRBquJBk4AfSGXdtG064tSOAdeUaBcXf31t2', 'Bogor', 'Pemantau'),
(4, 'Jasmine', '$2y$10$c6U9o74OEGVPZSbQdfRBquJBk4AfSGXdtG064tSOAdeUaBcXf31t2', 'Bali', 'Pemantau'),
(5, 'Justine', '$2y$10$c6U9o74OEGVPZSbQdfRBquJBk4AfSGXdtG064tSOAdeUaBcXf31t2', 'Bandung', 'Pemantau'),
(6, 'Bagoes', '$2y$10$NIEsOIbnmHsIccZag.rTc.lq8TABGpSB8IdqnksP4dumgK0Q1X2YC', 'Bogor', 'Pengentri');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tabel_pemantauan`
--

CREATE TABLE `tabel_pemantauan` (
  `id_pemantauan` int(11) NOT NULL,
  `id_lokasi` int(11) NOT NULL,
  `data_curah_hujan` int(11) NOT NULL,
  `data_tekanan_udara` int(11) NOT NULL,
  `waktu` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tabel_pemantauan`
--

INSERT INTO `tabel_pemantauan` (`id_pemantauan`, `id_lokasi`, `data_curah_hujan`, `data_tekanan_udara`, `waktu`) VALUES
(289, 103, 1144, 992, '2019-11-07 16:55:10'),
(290, 100, 1163, 892, '2019-11-07 16:56:33'),
(291, 100, 1197, 230, '2019-11-07 16:56:44'),
(292, 100, 1217, 886, '2019-11-07 16:56:54'),
(293, 100, 1177, 321, '2019-11-07 16:57:03');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `tabel_lokasi`
--
ALTER TABLE `tabel_lokasi`
  ADD PRIMARY KEY (`id_lokasi`),
  ADD KEY `id_operator` (`id_operator`);

--
-- Indeks untuk tabel `tabel_operator`
--
ALTER TABLE `tabel_operator`
  ADD PRIMARY KEY (`id_op`);

--
-- Indeks untuk tabel `tabel_pemantauan`
--
ALTER TABLE `tabel_pemantauan`
  ADD PRIMARY KEY (`id_pemantauan`),
  ADD KEY `id_lokasi` (`id_lokasi`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `tabel_pemantauan`
--
ALTER TABLE `tabel_pemantauan`
  MODIFY `id_pemantauan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=294;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `tabel_lokasi`
--
ALTER TABLE `tabel_lokasi`
  ADD CONSTRAINT `id_operator` FOREIGN KEY (`id_operator`) REFERENCES `tabel_operator` (`id_op`) ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `tabel_pemantauan`
--
ALTER TABLE `tabel_pemantauan`
  ADD CONSTRAINT `id_lokasi` FOREIGN KEY (`id_lokasi`) REFERENCES `tabel_lokasi` (`id_lokasi`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
